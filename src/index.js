import Server from './server.js';
import conf from './conf/main';
import iglooControl from './util/igloo-control.js';
import utilities from "./util/utilities.js";

const igloo = new iglooControl(conf.control,utilities);
const serv = new Server(conf,utilities,igloo);
//save the updated metatdata every 5 minutes
setInterval(()=>igloo.save(),utilities.minToMs(0.25));
serv.rebuildContent();
serv.open();