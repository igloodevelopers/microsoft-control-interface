const fs = require('fs');
const path = require('path');
exports.FloatMap = function (val, low1, high1, low2, high2) {
	return (low2 + (val - low1) * (high2 - low2) / (high1 - low1));
}

exports.parseOSCPlayback = function (msg, pinfo) {
	var retTemp = pinfo;
	msg.forEach(element => {
		if (element[0] == '/mov/currentMovieName') {
			retTemp.movieName = element[1];
		}
		if (element[0] == '/mov/playhead') {
			retTemp.playHead = element[1];
			retTemp.fullTime = element[2];
		}
		if (element[0] == '/mov/pause') {
			if (element[1] == 0) {
				retTemp.pause = false;
			} else {
				retTemp.pause = true;
			}
		}
	});
	return retTemp;
	//console.log(retTemp);
}
exports.minToMs = (min) => {
	return (min * 60 * 1000);
}
exports.copyObject = (object) => {
	return JSON.parse(JSON.stringify(object));
}
/**
 * Explores recursively a directory and returns all the filepaths and folderpaths in the callback.
 * 
 * @see http://stackoverflow.com/a/5827895/4241030
 * @param {String} path 
 * @param {Boolean} mode
 * @param {Function} done 
 */
exports.traverseFolders = (dir, mode, done) => {
	var results;
	if(mode){
		results = {"root":[]};
	} else {
		results = [];
	}

	fs.readdir(dir, function (err, list) {
		if (err) return done(err);
		var pending = list.length;
		if (!pending) return done(null, results);
		list.forEach(function (_file) {
			let filename = _file;
			let file = path.resolve(dir, _file);
			fs.stat(file, function (err, stat) {
				// If directory, execute a recursive call
				if (stat && stat.isDirectory() && mode) {
					// Add directory to array [comment if you need to remove the directories from the array]
					//results.push(file);
					results[filename] = [];
					exports.traverseFolders(file,false, function (err, res) {
						results[filename] = res;
						if (!--pending) done(null, results);
					});
				} else {
					if(filename.charAt(0) == '.'){
						console.log("hidden file, skipping");
					} else {
						if(mode){
							results.root.push(filename);
						} else {
							results.push(filename);
						}
					}
					if (!--pending) done(null, results);
				}
			});
		});
	});
}