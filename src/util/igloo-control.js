import OSC from "osc-js";
import fs from "fs";
import EventEmitter from 'events';
export default class extends EventEmitter{
    constructor(settings,utilities){
        super();
        this.clips = {};
        this.settings = settings;
        this.utilities = utilities;
        this.load();
    }
    load = () => {
        fs.readFile(".conf/clipMetadata.json",(err,data)=>{
            if(err){
                console.log(err);
            } else {
                try{
                    this.clips = JSON.parse(data);
                } catch (e) {
                    if(e){
                        console.log('Error reading Clip Metadata, rebuilding...');
                        this.clips = {}
                        fs.writeFile('.conf/clipMetadata.json','{}',(err)=> {
                            if(err) console.log(err)
                        });
                    }
                }
                
            }
        });
    }
    save = () => {
      var output = JSON.stringify(this.clips);
      fs.writeFile('.conf/clipMetadata.json',output,(err)=>{
        if(err) console.log(err);
      });
    }
    queryClip = (clipname) =>{
        if(this.clips[clipname] == null){
            this.clips[clipname] = {
                "yPosOffset":0.0,
                "wrapPosition":0.0,
                "zoomPositionVert":0.0,
                "zoomPosition":0.0,
                "volume":0.75,
                "isLooping":false
            };
            console.log(this.clips);
            
            return this.settings.defaultClipData;
        } else {
            return this.clips[clipname];
        }
    }
    updateClip = (clipname,updatedArray)=>{
        this.clips[clipname] = updatedArray;
        console.log(updatedArray);
    }
    
}


