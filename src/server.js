import EventEmitter from 'events';
import express from 'express';
import http from 'http';
import path from 'path';
import fs from 'fs';
import OSC from 'osc-js'

export default class extends EventEmitter{
    constructor(settings,utilities,iglooControl){
        super();
        this.utilities = utilities;
        this.settings = settings;
        this.iglooControl = iglooControl;
        this.osc = new OSC(settings.osc.connection);
        this.state={
            "playback":{},
            "clipbank":[],
            "possibleValues":[],
            "holding":0,
            "playing":-1,
            "toggles":{
                "unreal":false,
                "pause":false
            },
            "playhead":0,
            "clipMetadata":{},
            "currentClip":""
        }
        fs.readFile(".conf/clipbank.json",(err,data)=>{
            if(err){
                console.log(err);
            } else {
                try{
                    this.state.clipbank = JSON.parse(data);
                } catch (e) {
                    console.log('Error loading clipbank, rebuilding...');
                    this.state.clipbank = [];
                    fs.writeFile('.conf/clipbank.json', '[]',(err)=> {
                        if(err) console.log(err)
                    });
                }
                
                this.state.possibleValues = [];
                for (let i = 0; i < this.state.clipbank.length; i++) {
                    if(this.state.clipbank[i] != null){
                        this.state.possibleValues.push(i);
                    }
                }
            }
        });
        this.content={};
        this.app = new express();
        this.router = new express.Router();
        //toggles
        this.router.route("/sendOSC/:command").get((req,res)=>{
            switch (req.params.command) {
                case "ShowStateOn":
                    this.osc.send(new OSC.Message(settings.osc.patterns.ShowState.address,settings.osc.patterns.ShowState.on));
                    break;
                case "ShowStateOff":
                    this.osc.send(new OSC.Message(settings.osc.patterns.ShowState.address,settings.osc.patterns.ShowState.off));
                    break;
                case "SmartGlassOn":
                    this.osc.send(new OSC.Message(settings.osc.patterns.SmartGlass.address,settings.osc.patterns.SmartGlass.on));
                    break;
                case "SmartGlassOff":
                    this.osc.send(new OSC.Message(settings.osc.patterns.SmartGlass.address,settings.osc.patterns.SmartGlass.off));
                    break;

                case "movieOn":
                    this.osc.send(new OSC.Message(settings.osc.patterns.movie.address,settings.osc.patterns.movie.on));
                    break;
                case "movieOff":
                    this.osc.send(new OSC.Message(settings.osc.patterns.movie.address,settings.osc.patterns.movie.off));
                    break;

                case "warpingOn":
                    this.osc.send(new OSC.Message(settings.osc.patterns.warp.address,settings.osc.patterns.warp.on));
                    break;
                case "warpingOff":
                    this.osc.send(new OSC.Message(settings.osc.patterns.warp.address,settings.osc.patterns.warp.off));
                    break;

                case "blendingOn":
                    this.osc.send(new OSC.Message(settings.osc.patterns.blending.address,settings.osc.patterns.blending.on));
                    break;
                case "blendingOff":
                    this.osc.send(new OSC.Message(settings.osc.patterns.blending.address,settings.osc.patterns.blending.off));
                    break;

                case "ProjectorShutterOn":
                    this.osc.send(new OSC.Message(settings.osc.patterns.ProjectorShutter.address,settings.osc.patterns.ProjectorShutter.on));
                    break;
                case "ProjectorShutterOff":
                    this.osc.send(new OSC.Message(settings.osc.patterns.ProjectorShutter.address,settings.osc.patterns.ProjectorShutter.off));
                    break;
                case "ProjectorStandbyOn":
                    this.osc.send(new OSC.Message(settings.osc.patterns.ProjectorStandby.address,settings.osc.patterns.ProjectorStandby.on));
                    break;
                case "ProjectorStandbyOff":
                    this.osc.send(new OSC.Message(settings.osc.patterns.ProjectorStandby.address,settings.osc.patterns.ProjectorStandby.off));
                    break;  
                case "BlindsOn":
                    this.osc.send(new OSC.Message(settings.osc.patterns.Blinds.address,settings.osc.patterns.Blinds.on));
                    break;
                case "BlindsOff":
                    this.osc.send(new OSC.Message(settings.osc.patterns.Blinds.address,settings.osc.patterns.Blinds.off));
                    break;
                case "HouseLightsOn":
                    this.osc.send(new OSC.Message(settings.osc.patterns.HouseLights.address,settings.osc.patterns.HouseLights.on));
                    break;
                case "HouseLightsOff":
                    this.osc.send(new OSC.Message(settings.osc.patterns.HouseLights.address,settings.osc.patterns.HouseLights.off));
                    break;
                case "PrevTrack":
                    if(this.state.possibleValues != []){
                        var possibleValuePlaying = this.state.possibleValues.findIndex((element)=> {return element == this.state.playing;})
                        if(possibleValuePlaying-1<0){
                            this.triggerClip(this.state.possibleValues[this.state.possibleValues.length-1]);
                        } else {
                            this.triggerClip(this.state.possibleValues[possibleValuePlaying-1]);
                        }
                    }
                    
                    break;

                case "NxtTrack":
                    if(this.state.possibleValues != []){
                        var possibleValuePlaying = this.state.possibleValues.findIndex((element)=> {return element == this.state.playing;})
                        if(possibleValuePlaying+1==this.state.possibleValues.length){
                            this.triggerClip(this.state.possibleValues[0]);
                        } else {
                            this.triggerClip(this.state.possibleValues[possibleValuePlaying+1]);
                        }
                    }
                    break;

                case "unrealToggle":
                    this.osc.send(new OSC.Message(settings.osc.patterns.geToggle.address[0],"UnrealCam"));
                    this.osc.send(new OSC.Message(settings.osc.patterns.geToggle.address[1],this.state.toggles.unreal?0:1));
                    this.osc.send(new OSC.Message(settings.osc.patterns.geToggle.address[2],1));
                    this.state.toggles.unreal = !this.state.toggles.unreal;
                    break;
                case "displayInfoPanel":
                    this.osc.send(new OSC.Message("/disp/displayInfoPanel",1));
                    break;
                case "displayControlPoints":
                    this.osc.send(new OSC.Message("/disp/displayControlPoints",1));
                    break;
                case "displayTestImage":
                    this.osc.send(new OSC.Message("/disp/displayTestImage",1));
                    break;

                
                case "load":
                    this.osc.send(new OSC.Message(settings.osc.patterns.load.address,1));
                    break;
                case "save":
                    this.osc.send(new OSC.Message(settings.osc.patterns.save.address,1));
                    break;

                case "Play":
                    this.osc.send(new OSC.Message(settings.osc.patterns.Play.address,1));
                    break;
                
                case "Pause":
                    this.state.toggles.pause = !this.state.toggles.pause;
                    this.osc.send(new OSC.Message(settings.osc.patterns.Pause.address,this.state.toggles.pause?1:0));

                        break;

                case "toggleLoop":
                    var toSend = this.state.clipMetadata.isLooping?0:1;
                    this.state.clipMetadata.isLooping = !this.state.clipMetadata.isLooping;
                    this.iglooControl.updateClip(this.state.currentClip,this.state.clipMetadata);
                    this.osc.send(new OSC.Message(settings.osc.patterns.loop.address,toSend));
                    break;
                default:
                    this.osc.send(new OSC.Message("/"+req.params.command,1.0));
                    break;
            }
            console.log("Sent OSC "+req.params.command);
            res.end("Success!");

        });
        this.router.route("/slider/:command/:val/:min/:max").get((req,res)=>{
            if(settings.osc.patterns[req.params.command]!=null){
                var command = settings.osc.patterns[req.params.command]
                var val = utilities.FloatMap(parseInt(req.params.val),parseInt(req.params.min),parseInt(req.params.max),command.min,command.max);
                if(req.params.command == "volume"){
                    this.state.clipMetadata.volume = val;
                    this.iglooControl.updateClip(this.state.currentClip,this.state.clipMetadata);
                }
                if(req.params.command == "wrap"){
                    this.state.clipMetadata.wrapPosition = val;
                    this.iglooControl.updateClip(this.state.currentClip,this.state.clipMetadata);
                }
                if(req.params.command == "yPosOffset"){
                    this.state.clipMetadata.yPosOffset = val;
                    this.iglooControl.updateClip(this.state.currentClip,this.state.clipMetadata);
                }
                if(req.params.command == "zoomPositionVert"){
                    this.state.clipMetadata.zoomPositionVert = val;
                    this.iglooControl.updateClip(this.state.currentClip,this.state.clipMetadata);
                }
                this.osc.send(new OSC.Message(command.address,val));
                console.log("sent command: "+command.address+val);
            } else {
                console.log("invalid command:" +req.params.command);
            }
            res.end("success");
        });
        this.router.route("/clip/set/:number/:name").get((req,res)=>{
            if(req.params.name == "null"){
                this.state.clipbank[parseInt(req.params.number)] = null;
            } else {
                this.state.clipbank[parseInt(req.params.number)] = req.params.name;
            }
            
            this.state.possibleValues = [];
            for (let i = 0; i < this.state.clipbank.length; i++) {
                if(this.state.clipbank[i] != null){
                    this.state.possibleValues.push(i);
                }
            }
            var json = JSON.stringify(this.state.clipbank);
            fs.writeFile('.conf/clipbank.json', json,(err)=>{
                console.log(err);
            });
            res.json(this.state.clipbank);
        });
        this.router.route("/clip/holding/set/:number").get((req,res)=>{
            this.state.holding = parseInt(req.params.number);
            res.end("success");
        });
        this.router.route("/clip/holding/trigger").get((req,res)=>{
            this.triggerClip(this.state.holding);
            res.end("success");
        });
        this.router.route("/screen/:number").get((req,res)=>{
            this.osc.send(new OSC.Message(settings.osc.patterns.screen.address+"/"+settings.osc.patterns.screen.screens[parseInt(req.params.number)]));
            console.log("triggered screen " + req.params.number);
            res.end("success");
        })
        this.router.route("/clip/trigger/:number").get((req,res)=>{
            if(this.state.toggles.unreal){
                this.osc.send(new OSC.Message(settings.osc.patterns.geToggle.address,0.0));
                this.state.toggles.unreal = false;
            }
           this.triggerClip(parseInt(req.params.number));
           res.end("success");
        });
        this.router.route("/status").get((req,res)=>{
            var response = {
                "content":this.content,
                "state":this.state,
                "sliderTransformed":{
                    "wrapPosition":utilities.FloatMap(this.state.clipMetadata.wrapPosition,this.settings.osc.patterns.wrapPosition.min,this.settings.osc.patterns.wrapPosition.max,0,100),
                    "volume":utilities.FloatMap(this.state.clipMetadata.volume,this.settings.osc.patterns.volume.min,this.settings.osc.patterns.volume.max,0,100),
                    "zoomPositionVert":utilities.FloatMap(this.state.clipMetadata.zoomPositionVert,this.settings.osc.patterns.zoomPositionVert.min,this.settings.osc.patterns.zoomPositionVert.max,0,100),
                    "yPosOffset":utilities.FloatMap(this.state.clipMetadata.yPosOffset,this.settings.osc.patterns.yPosOffset.min,this.settings.osc.patterns.yPosOffset.max,0,100),


                }
            }
            res.json(response);
        });
        this.router.route("/content/refresh").get((req,res)=>{
            res.json(this.rebuildContent());
            
        });
        this.app.use("/api",this.router);
        this.osc.on("/mov/moviePlayhead", (playhead) => {
            this.state.playhead = playhead.args[0];
        });
    }
    open = () =>{
        http.createServer(this.app).listen(this.settings.api.port);
        console.log(`Service listening on port ${this.settings.api.port}`);
        this.osc.open();
    }
    triggerClip = (clip) =>{
        if(this.state.clipbank[clip] != null){
            this.osc.send(new OSC.Message(this.settings.osc.patterns.clip.address,this.state.clipbank[clip]));
            console.log("successfully triggered "+this.state.clipbank[clip]);
            this.state.playing = clip;
            this.state.currentClip = this.state.clipbank[clip];
            this.state.clipMetadata = this.iglooControl.queryClip(this.state.currentClip);
            this.sendClipMetadata(this.state.clipMetadata);

        } else {
            console.log("no clip set for slot "+clip);
        }
    }
    sendClipMetadata = (metadata) =>{
        var self = this;
        Object.keys(metadata).forEach((key)=>{
            var command = this.settings.osc.patterns[key];
            if(command!=null) {
                var sendable;
                if(command.address == "/mov/setLoop"){
                    if(metadata[key]){
                        sendable=1;
                    } else {
                        sendable=0;
                    }
                } else {
                    sendable=metadata[key]
                }
                self.osc.send(new OSC.Message(command.address,sendable));
            }
            
        })
    }
    rebuildContent = () =>{
        this.utilities.traverseFolders(this.settings.content.folder,true,(err,res)=>{
            this.content = res;
        });
    }
}