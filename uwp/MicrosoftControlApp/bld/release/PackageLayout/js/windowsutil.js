﻿var ViewManagement = Windows.UI.ViewManagement;
var FullScreenSystemOverlayMode = ViewManagement.FullScreenSystemOverlayMode;
var ApplicationView = ViewManagement.ApplicationView;
var localSettings = Windows.Storage.ApplicationData.current.localSettings;

//set for uwp or browser

function setupWinRT() {    
    if (typeof (Storage) !== "undefined") {
        if (localStorage.getItem("address") == null) {
            localStorage.setItem("address", state.address);
        } else {
            state.address = localStorage.getItem("address");
            console.log("pulled address " + state.address);
        }
    } else {
        console.log("Sorry, your browser does not support Web Storage...");
    }
    var view = ApplicationView.getForCurrentView();
    //try and go fullscreen
    view.tryEnterFullScreenMode()
    console.log("fullscreen");

    //setup the title bar to adhere to the colors of the app, not the system
    view.titleBar.backgroundColor = Windows.UI.Colors.black;
    view.titleBar.foregroundColor = Windows.UI.Colors.grey;
    view.titleBar.buttonBackgroundColor = Windows.UI.Colors.black;
    view.titleBar.buttonForegroundColor = Windows.UI.Colors.grey;

}

function setupRESTApi() {
    if (state.uwp) {
        state.refreshTimeout = setTimeout(refreshWindows(), 3000);
    }
}