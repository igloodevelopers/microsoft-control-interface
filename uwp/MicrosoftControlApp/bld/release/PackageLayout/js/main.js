﻿// Your code here!
var state = {
    "connection": {
        "offline":true,
        "previousTick":false
    },
    "uwp": true,
    "refreshTimeout":null,
    "serverResponse":null,
    "currentPage": null,
    "currentSubPage": null,
    "currentSubPageButton": null,
    "editClips": false,
    "currentEditedClip": 0,
    "holdingSlideClip": 0,
    "clipnames": [],
    "address":"http://127.0.0.1:8094/api/",
    "volume":false,
    "looping":true,
    "volumeSliderUpdateEnable":true,
    "playbackSliderUpdateEnable":true,
    sliders:{},
    "clipMetadata":{
        "yPosOffset":0.0,
        "wrapPosition":0.0,
        "zoomPositionVert":0.0,
        "zoomPosition":0.0,
        "volume":0.75,
        "isLooping":false
    }
}
window.onload = function () {
    window.setInterval(status, 750);

    status();
    try {
        setupWinRT();
    } catch (err) {
        uwp = false;
    }
    //setupRESTApi();
    $(".button").click(function() {
        buttonClicked($(this));
    });
    $(".sliderControl").on('change',function(){
        sendCommand("slider/"+$(this).data("command")+"/"+$(this).val()+"/1/100");
    });
    
    $(".screen").click(function(){
        sendCommand("screen/"+$(this).data("screen"));
    });
    $("#clips").click(function () {
        console.log($(this));
        toggleEdit();
    });
    $(".contentGrid").click(function () {
        clipbutton($(this));
        //get clipinfo
    });
    $("#volume").click(function(){
        if(state.volume){
            $(this).removeClass("selected");
            $(".playback").removeClass("dimmed");
            $(".volumeslider").addClass("hide");
        } else {
            $(this).addClass("selected");
            $(".playback").addClass("dimmed");
            $(".volumeslider").removeClass("hide");
        }
        state.volume = !state.volume;
    });
    $(".collectionButton").click(function(){
        $(".clipCollection").addClass("hidden");
        $($(this).data("page")).removeClass("hidden");
    });
    $("#clearButton").click(function(){
        if(!state.editClips){
            return;
        } 
        sendCommand("clip/set/" + state.currentEditedClip + "/null");
        state.clipnames[state.currentEditedClip] = null;
        updateClipbank();
        $(".list-group-item").removeClass("active");

    });
    $("#connectionstatus").click(function(){
        $("#serveraddress").toggleClass("hidden");
    });

    state.currentPage = $("#mainScreen");
    state.currentSubPage = $("#advanced");
    state.currentSubPageName = "#advanced";
    state.currentSubPageButton = $("#advancedbutton");
    $(".pageSwitch").click(function() {
        console.log($(this));
        changePage($(this).data("page"));
    });
    $(".settingsTab").click(function () {
        changeSubPage($(this).data("page"), $(this));
    });
    $("#servaddressbutton").click(function () {
        changeAddress();
    });

    $("#holdingslide").change(function(){
        if($(this).is(':checked')){
            state.holdingSlideClip = state.currentEditedClip;
        } else {
            state.holdingSlideClip = -1;
        }
        sendCommand("clip/setHolding/"+state.holdingSlideClip);
    });
    $("#servaddress").val(state.address);
}

function buttonClicked(button) {
    if(button.data("command")=="toggleLoop"){
        console.log("loop");
        if(state.looping){
            button.removeClass("looping");
        }else{
            button.addClass("looping");
        }
        state.looping=!state.looping;
    }
    
    if(button.data("command") == "holdSlide"){
        sendCommand("/clip/holding/trigger");
    } else {
        sendCommand("sendOSC/"+button.data("command"));
    }
}
function changePage(newPage){
    $(newPage).removeClass("hidden");
    state.currentPage.addClass("hidden");
    state.currentPage = $(newPage);
}
function changeSubPage(newPage, button) {
    if(newPage!=state.currentSubPageName){
        $(newPage).removeClass("hidden");
        state.currentSubPageButton.removeClass("active");
        button.addClass("active");
        state.currentSubPage.addClass("hidden");
        state.currentSubPage = $(newPage);
        state.currentSubPageButton = button; 
        state.currentSubPageName = newPage;
    }
}
function toggleEdit() {
    console.log("switching edit");
    if (state.editClips) {
        $("#clipview").removeClass("dark");
        $("#clipConfig").addClass("hidden");
        $("#editclipstext").addClass("hidden");
        var clipname = "#clip" + state.currentEditedClip;
        $(clipname).removeClass("select");
        window.setTimeout(function(){
            $("#clipConfig").addClass("addDisplay");
        },500);
    } else {
        $("#clipConfig").removeClass("addDisplay");
        $("#clipview").addClass("dark");
        $("#editclipstext").removeClass("hidden");
        state.currentEditedClip = 0;
        $("#clip0").addClass("select");
        window.setTimeout(function(){
            $("#clipConfig").removeClass("hidden");
        },200);
        if(!state.connection.offline){
            $.getJSON(state.address+"content/refresh").done(function(data){
                
            });
        }
    }
    state.editClips = !state.editClips;
}

function clipbutton(button) {
    if(parseInt(button.data("clip"))==-1){
        //unreal
        if(state.editClips){
            return;
        } else {
            sendCommand("sendOSC/unrealToggle");
            return;
        }
    }
    if (state.editClips) {
        var clipname = "#clip" + state.currentEditedClip;
        $(clipname).removeClass("select");
        button.addClass("select");
        state.currentEditedClip = parseInt(button.data("clip"));
        $(".list-group-item").removeClass("active");
        $(".list-group-item").each(function(){
            if(state.clipnames[state.currentEditedClip] == $(this).data("asset")){
                $(this).addClass("active");
            }
        });
        if(state.holdingSlideClip == state.currentEditedClip){
            $("#holdingslide").prop("checked",true);
        } else {
            $("#holdingslide").prop("checked",false);
        }
    } else {
        sendCommand("clip/trigger/" + button.data("clip"));
    }
}

function changeAddress() {
    state.address = $("#servaddress").val();
    localStorage.setItem("address", state.address);
    console.log(state.address)
    var dialog = new Windows.UI.Popups.MessageDialog("Changed Address to " + state.address);
    dialog.showAsync();
    status();
}

function updateClipbank() {

    $(".contentGrid").each(function () {
        var element = $(this);
        if(parseInt(element.data("clip"))<0){
            return;
        }
        if (state.clipnames[parseInt($(this).data("clip"))] != null) {
            element.text(state.clipnames[parseInt($(this).data("clip"))]);
        } else {
            element.text("");
        }
    });
}
function checkConnection(){
    if(state.connection.offline && !state.connection.previousTick){
        $("#disconnectpopup").removeClass("hidden");
        window.setTimeout(function(){$("#disconnectpopup").addClass("hidden")},5000);
        $("#connectionstatus").html("<span>&#xF384;</span>");
    }
    if(!state.connection.offline && state.connection.previousTick){
        $("#connectionstatus").html("<span>&#xF385;</span><span>&#xF386;</span>");
    }
    state.connection.previousTick = state.connection.offline;
    
}
function status() {
    $.getJSON(state.address + "status").done(function (data) {
        var assetgroup = "";
        data.content.forEach(function (content) {
            var classaddition = "";
            if(content == state.clipnames[state.currentEditedClip]){
                classaddition = " active";
            }
            assetgroup += '<li class="list-group-item'+ classaddition +'" data-asset="' + content + '">' + content + '</li>';
        });
        $("#assetgroup").html(assetgroup);
        $(".list-group-item").click(function () {
            asset = $(this).data("asset");
            sendCommand("clip/set/" + state.currentEditedClip + "/" + asset);
            state.clipnames[state.currentEditedClip] = asset;
            updateClipbank();
            $(".list-group-item").removeClass("active");
            $(this).addClass("active");
        });
        state.connection.offline = false;
        state.clipnames = data.state.clipbank;
        state.clipMetadata = data.state.clipMetadata;
        state.playhead = data.state.playhead;
        state.sliders = data.sliderTransformed;
        updateVisualState();
        updateClipbank();
    }).fail(function (jxqr, textStatus, error) {
        state.connection.offline = true;
        var err = textStatus + ", "  + error;
        console.log("Request Failed: " + err);
        console.log(jxqr);
        return false;
    });
    checkConnection();
}
function updateVisualState(){
    //playhead
    if(state.playbackSliderUpdateEnable){
        $("#playhead").val(state.playhead*100);
    }
    
    //loop
    if(state.clipMetadata.isLooping){
        $("#loop").addClass("looping");
    } else {
        $("#loop").removeClass("looping");
    }
    //volume
    if(state.volumeSliderUpdateEnable){
        $("#volumeSlider").val(state.clipMetadata.volume*100);
    }
    $("#zoomPos").val(state.sliders.zoomPositionVert);
    $("#yPos").val(state.sliders.yPosOffset);
    $("#wrapPosSlide").val(state.sliders.wrapPosition);
}