﻿var ViewManagement = Windows.UI.ViewManagement;
var FullScreenSystemOverlayMode = ViewManagement.FullScreenSystemOverlayMode;
var ApplicationView = ViewManagement.ApplicationView;
var localSettings = Windows.Storage.ApplicationData.current.localSettings;

//set for uwp or browser

function setupWinRT() {
    //check to see if local settings have been created, if not, create them
    if (localSettings.values["setup"] == null) {
        var connection = new Windows.Storage.ApplicationDataCompositeValue();
        connection["address"] = defaults.connection.address;
        connection["attempts"] = defaults.connection.attempts;
        localSettings["connection"] = connection;
        localSettings["setup"] = true;
    }
    state.connection.settings = localSettings["connection"];
    
    var view = ApplicationView.getForCurrentView();
    //try and go fullscreen
    view.tryEnterFullScreenMode()
    console.log("fullscreen");

    //setup the title bar to adhere to the colors of the app, not the system
    view.titleBar.backgroundColor = Windows.UI.Colors.white;
    view.titleBar.foregroundColor = Windows.UI.Colors.black;
    view.titleBar.buttonBackgroundColor = Windows.UI.Colors.white;
    view.titleBar.buttonForegroundColor = Windows.UI.Colors.black;

}

function setupRESTApi() {
    if (state.uwp) {
        state.refreshTimeout = setTimeout(refreshWindows(), 3000);
    }
}

function refreshWindows() {
    console.log("refreshing");
    var success = true;
    try {
        state.connection.serverResponse = $.getJSON(state.connection.settings["address"]+"state");
    } catch (err) {
        if (state.connection.currentAttempt == 0) {
            $("#Disconnected").removeClass("hidden");
        } else if (state.connection.currentAttempt == state.connection.settings["attempts"]) {
            changePage($("#DisconnectedFromServer"));
            $("#Disconnected").addClass("hidden");
            clearTimeout(state.refreshTimeout);
        }
        success = false;
    }
    if (success && currentAttempt == state.connection.settings["attempts"]) {
        changePage($("#mainScreen"));
        state.connection.currentAttempt = 0;
    } else if (success){
        state.connection.currentAttempt = 0;
        $("#Disconnected").addClass("hidden");
    }
}