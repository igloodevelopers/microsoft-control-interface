﻿// Your code here!
var state = {
    "connection": {
        "currentAttempt": 0,
        "error": ""
    },

    "uwp": true,
    "refreshTimeout":null,
    "serverResponse":null,
    "currentPage": null,
    "currentSubPage": null,
    "server":"http://192.168.1.1"
}
window.onload = function () {
    try {
        setupWinRT();
    } catch (err) {
        uwp = false;
    }
    //setupRESTApi();
    $(".button").click(function() {
        buttonClicked($(this));
    });
    state.currentPage=$("#mainScreen");
    $(".pageSwitch").click(function() {
        console.log($(this));
        changePage($(this).data("page"));
    });
    //fillContent();
}

function buttonClicked(button) {
    //sendCommand("buttons/"+button.data("command"));
}
function changePage(newPage){
    $(newPage).removeClass("hidden");
    state.currentPage.addClass("hidden");
    state.currentPage = $(newPage);
}